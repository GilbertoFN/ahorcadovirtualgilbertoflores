/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AhorcadoVirtualGilbertoFlores;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Gilberto FN
 */
public class Ahorcado extends JPanel {

    private int x;
    private int y;
    private int fails = 0;
    private int cant;

    private int fail = 0;
    private int pos = 0;

    private boolean reiniciar;

    private int con = 0;
    private int veces = 0;
    private int conta = 0;
    private String[] letras = new String[10];
    private String[] letrasFails = new String[14];

    private boolean win;

    private int conLetras = 0;
    private char intento;
    private int xx;

    private String[] LETRAS = new String[10];

    private LinkedList<Vida> vidas;

    public Ahorcado(int x, int y) {

        vidas = new LinkedList<>();
        vidas.add(new Vida(30, 30));
        vidas.add(new Vida(80, 30));
        vidas.add(new Vida(130, 30));
        vidas.add(new Vida(30, 80));
        vidas.add(new Vida(80, 80));
        vidas.add(new Vida(130, 80));

        this.x = x;
        this.y = y;
        try {

            img0 = ImageIO.read(new File("imagenes/ahorcado0.jpg"));
            img1 = ImageIO.read(new File("imagenes/ahorcado1.jpg"));
            img2 = ImageIO.read(new File("imagenes/ahorcado2.jpg"));
            img3 = ImageIO.read(new File("imagenes/ahorcado3.jpg"));
            img4 = ImageIO.read(new File("imagenes/ahorcado4.jpg"));
            img5 = ImageIO.read(new File("imagenes/ahorcado5.jpg"));
            img6 = ImageIO.read(new File("imagenes/ahorcado6.jpg"));

        } catch (IOException e) {
            System.out.println("Problemas leyendo la imagen n");
            System.out.println("Motivo: " + e.getLocalizedMessage());
        }

    }

    BufferedImage img0;
    BufferedImage img1;
    BufferedImage img2;
    BufferedImage img3;
    BufferedImage img4;
    BufferedImage img5;
    BufferedImage img6;

    public int preguntar() {
        int op = 0;
        int input = JOptionPane.showConfirmDialog(null, "¿Desea reiniciar el juego?",
                "YES_NO_OPTION", JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE);

        return op;
    }

    public void reiniciar() {
        fails = 0;
        cant = 0;
        fail = 0;
        pos = 0;
        con = 0;
        veces = 0;
        conta = 0;
        letras = new String[10];
        letrasFails = new String[20];
        win = false;
        conLetras = 0;
        LETRAS = new String[10];
    }

    public void paint(Graphics g) {
        //fondo
        g.setColor(new Color(36, 155, 159));//turqueza
        g.fillRect(0, 0, 1000, 1000);
        g.setColor(Color.BLACK);
        g.fillRect(20, 20, 500, 350);
        g.setColor(Color.WHITE);
        g.fillRect(25, 25, 490, 340);

        g.setColor(Color.RED);
        Font f2 = new Font("Arial", Font.BOLD, 45);
        g.setFont(f2);
        for (int i = 0; i < letrasFails.length; i++) {
            if (letrasFails[i] != null) {
                switch (i + 1) {
                    case 1:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 535, 52);
                        break;
                    case 2:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 620, 52);
                        break;
                    case 3:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 535, 134);
                        break;
                    case 4:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 620, 134);
                        break;
                    case 5:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 535, 216);
                        break;
                    case 6:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 620, 216);
                        break;
                    case 7:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 535, 316);
                        break;
                    case 8:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 620, 316);
                        break;
                    case 9:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 535, 380);
                        break;
                    case 10:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 620, 380);
                        break;
                    case 11:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 535, 462);
                        break;
                    case 12:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 620, 462);
                        break;
                    case 13:
                        g.drawString(String.valueOf(letrasFails[i]).toUpperCase(), 535, 531);
                        break;
                }

            }

        }
        if (fails < 6) {
            for (Vida vida : vidas) {
                vida.paint(g);
            }
            pintarEspacios(g);
        }
        if (win) {

//            int input = JOptionPane.showConfirmDialog(null, "¿Desea reiniciar el juego?",
//                    "YES_NO_OPTION", JOptionPane.YES_NO_OPTION,
//                    JOptionPane.INFORMATION_MESSAGE);
//preguntar();
            g.setColor(Color.BLACK);
            Font f = new Font("Arial", Font.BOLD, 29);
            g.setFont(f);

            g.drawString("<FELICITACIONES HAS GANADO>", 33, 350);
//            reiniciar = true;

        }
        if (fails == 1) {
            g.drawImage(img0, 300, 40, 150, 250, this);
            g.setColor(Color.WHITE);
            g.fillRect(130, 80, 50, 50);
        } else if (fails == 2) {
            g.drawImage(img1, 300, 40, 150, 250, this);
            g.setColor(Color.WHITE);
            g.fillRect(130, 80, 50, 50);
            g.fillRect(80, 80, 50, 50);
        } else if (fails == 3) {
            g.drawImage(img2, 300, 40, 150, 250, this);
            g.setColor(Color.WHITE);
            g.fillRect(130, 80, 50, 50);
            g.fillRect(80, 80, 50, 50);
            g.fillRect(30, 80, 50, 50);
        } else if (fails == 4) {
            g.drawImage(img3, 300, 40, 150, 250, this);
            g.setColor(Color.WHITE);
            g.fillRect(130, 80, 50, 50);
            g.fillRect(80, 80, 50, 50);
            g.fillRect(30, 80, 50, 50);
            g.fillRect(130, 30, 50, 50);
        } else if (fails == 5) {
            g.drawImage(img4, 300, 40, 150, 250, this);
            g.setColor(Color.WHITE);
            g.fillRect(130, 80, 50, 50);
            g.fillRect(80, 80, 50, 50);
            g.fillRect(30, 80, 50, 50);
            g.fillRect(130, 30, 50, 50);
            g.fillRect(80, 30, 50, 50);
        } else if (fails >= 6) {
            g.drawImage(img6, 300, 40, 150, 250, this);
            g.setColor(Color.BLACK);
            Font f = new Font("Arial", Font.BOLD, 31);
            g.setFont(f);

            g.drawString("<SUERTE LA PRÓXIMA LOSER>", 30, 350);
        }

    }

    /**
     * Método que permite crear un número random dependiendo del largo de un
     * arreglo
     *
     * @param palabras el arreglo que queremos leer
     * @return un número entero random
     */
    public int numRandom(String[] palabras) {
        int con = 0;
        for (int i = 0; i < palabras.length; i++) {
            if (palabras[i] != null) {
                con++;
            }
        }
        int res = (int) (Math.random() * con) + 1;

        return res;
    }

    /**
     * Método para separar una palabra de un arreglo por letras
     *
     * @param palabras el arreglo de palabras
     * @param pos la posicion de la palabra en el arreglo
     * @return un arreglo con las letras separadas
     */
    public String[] separaPalabra(String[] palabras, int pos) {
        String palabra = "";
        for (int i = 0; i < palabras.length; i++) {
            palabra = palabras[pos - 1];
        }
        String[] letras = palabra.split("-");

        return letras;
    }

    public void pintarEspacios(Graphics g) {
        switch (cant) {
            case 4:
                g.fillRoundRect(x + 20, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 100, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 180, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 260, y + 480, 70, 5, 5, 5);
                break;
            case 5:
                g.fillRoundRect(x + 20, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 100, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 180, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 260, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 340, y + 480, 70, 5, 5, 5);
                break;
            case 6:
                g.fillRoundRect(x + 20, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 100, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 180, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 260, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 340, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 420, y + 480, 70, 5, 5, 5);
                break;
            case 7:
                g.fillRoundRect(x + 20, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 100, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 180, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 260, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 340, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 420, y + 480, 70, 5, 5, 5);
                g.fillRoundRect(x + 500, y + 480, 70, 5, 5, 5);
                break;
        }

        for (int i = 0; i < letras.length; i++) {
            conLetras++;
            if (letras[i] != null) {
                g.setColor(Color.RED);
                Font f = new Font("Arial", Font.BOLD, 60);
                g.setFont(f);
                System.out.println("CONLETRAS-->" + i);
                switch (i + 1) {
                    case 1:
                        g.drawString(letras[i], 30, 480);
                        break;
                    case 2:
                        g.drawString(letras[i], 110, 480);
                        break;
                    case 3:
                        g.drawString(letras[i], 190, 480);
                        break;
                    case 4:
                        g.drawString(letras[i], 270, 480);
                        break;
                    case 5:
                        g.drawString(letras[i], 350, 480);
                        break;
                    case 6:
                        g.drawString(letras[i], 430, 480);
                        break;
                    case 7:
                        g.drawString(letras[i], 510, 480);
                        break;
                    case 8:
                        g.drawString(letras[i], 590, 480);
                        break;
                    case 9:
                        g.drawString(letras[i], 670, 480);
                        break;
                    case 10:
                        g.drawString(letras[i], 750, 480);
                        break;
                }
            }
        }
    }

    public void escoger(String[] palabras) {
        int con = 0;
        int pos = numRandom(palabras);
        String[] ll = separaPalabra(palabras, pos);
        for (int i = 0; i < ll.length; i++) {
            if (ll[i] != null) {
                LETRAS[con] = ll[i];
                con++;

            }
        }
        setCant(con);
    }

    public void validar(char letra) {
        if (!win) {
            for (int i = 0; i < LETRAS.length; i++) {
                if (LETRAS[i] != null) {
                    conta++;
                    if (String.valueOf(letra).toUpperCase().equals(LETRAS[i])) {
                        if (!(String.valueOf(letra).toUpperCase().equals(letras[i]))) {
                            letras[i] = LETRAS[i];
                            con++;
                        }
                    } else {
                        veces++;
                    }
                }
                if (con >= getCant()) {
                    win = true;
                }
            }
            if (!win) {
                if (veces == getCant()) {
                    letrasFails[pos]= String.valueOf(letra).toUpperCase();
                    pos++;
                    fail++;
                    veces = 0;
                }
                if (conta == getCant()) {
                    conta = 0;
                    veces = 0;
                }
            }
            sumarFails();
            fail = 0;
        }
    }

    public int sumarFails() {
        int f = 0;
        if (fail >= 1) {
            fails++;
            System.out.println("FAILs-->" + fails);
        }

        return f;
    }

    public boolean isReiniciar() {
        return reiniciar;
    }

    public void setReiniciar(boolean reiniciar) {
        this.reiniciar = reiniciar;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public int getXx() {
        return xx;
    }

    public void setXx(int xx) {
        this.xx = xx;
    }

    public char getIntento() {
        return intento;
    }

    public void setIntento(char intento) {
        this.intento = intento;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public int getFails() {
        return fails;
    }

    public void setFails(int fails) {
        this.fails = fails;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

}
