/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AhorcadoVirtualGilbertoFlores;

import javax.swing.JFrame;

/**
 *
 * @author Gilberto FN
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        JFrame frm = new JFrame("Ahorcado Virtual - v1");
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.add(new MiPanel());
        frm.pack();
        frm.setResizable(false);
        frm.setVisible(true);
        frm.setLocationRelativeTo(null);
        while (true) {
            frm.repaint();
            Thread.sleep(25);
            
        }
    }
}
