/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AhorcadoVirtualGilbertoFlores;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Gilberto FN
 */
public class MiPanel extends JPanel implements KeyListener {

    private Ahorcado juego;
    private Ahorcado pregunta;
    private String[] palabras = { "C-O-R-T-I-N-A"};//"A-V-I-O-N", "C-A-M-A", "C-O-M-I-D-A", "C-E-L-U-L-A-R", "P-A-L-A-B-R-A",
//    private String[] palabras = {"a-v-i-ó-n", "c-a-m-a", "c-o-m-i-d-a", "c-e-l-u-l-a-r", "p-a-l-a-b-r-a", "c-o-r-t-i-n-a"};

    private boolean iniciar;

    public MiPanel() {

        addKeyListener(this);
        setFocusable(true);
        setBackground(Color.WHITE);

        juego = new Ahorcado(0, 0);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(700, 500);
    }

    public void paint(Graphics g) {

        if (iniciar) {
            juego.paint(g);
        } else {
            g.setColor(Color.GREEN);
            g.fillRect(0, 0, 100000, 100000);
            g.setColor(Color.RED);
            Font f = new Font("Arial", Font.BOLD, 40);
            g.setFont(f);
            g.drawString("PRESS *ENTER* TO GAME", 300, 300);
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (!iniciar && e.getKeyCode() == KeyEvent.VK_ENTER) {
            juego.escoger(palabras);
            iniciar = true;
        }
        if (e.getKeyCode() != KeyEvent.VK_ENTER) {
            if (iniciar) {
                System.out.println("getKeyChar-->" + e.getKeyChar());
                juego.validar(e.getKeyChar());
                juego.setIntento(e.getKeyChar());
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

}
