/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AhorcadoVirtualGilbertoFlores;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Gilberto FN
 */
public class Vida {
    
    private int x;
    private int y;

    public Vida(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public void paint(Graphics g){
        int tam = 3;
        g.setColor(Color.BLACK);
        g.fillRect(x, y+tam, tam*8, tam*3);
        g.fillRect(x+tam, y, tam*2, tam*5);
        g.fillRect(x+tam*4, y, tam*3, tam*5);
        g.fillRect(x+tam*2, y+tam*4, tam*4, tam*2);
        g.fillRect(x+tam*3, y+tam*6, tam*2, tam*1);
        g.setColor(Color.red);
        g.fillRect(x+tam, y+tam, tam*2, tam*3);
        g.fillRect(x+tam, y+tam*2, tam*6, tam*2);
        g.fillRect(x+tam*4, y+tam, tam*3, tam*3);
        g.fillRect(x+tam*2, y+tam*4, tam*4, tam*1);
        g.setColor(new Color(136,0,0));
        g.fillRect(x+tam*3, y+tam*5, tam*2, tam*1);
        g.fillRect(x+tam*5, y+tam*4, tam*1, tam*1);
        g.fillRect(x+tam*6, y+tam, tam*1, tam*3);
        g.setColor(Color.WHITE);
        g.fillRect(x+tam, y+tam, tam*1, tam*2);
        
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    
    
    
}
